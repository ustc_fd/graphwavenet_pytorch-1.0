"""
A trainer class.
"""
import torch
from lib import metrics
from model.gwnetmodel import gwnet
import torch.optim as optim

class Trainer(object):
    def __init__(self,):
        self.optimizer =None
    def train(self,inputs,real_val):
        raise NotImplementedError

    def predict(self,inputs,real_val):
        raise NotImplementedError

    def update_lr(self, new_lr):
        for param_group in self.optimizer.param_groups:
            param_group['lr'] = new_lr

    def load(self, filename):
        try:
            checkpoint = torch.load(filename)
        except BaseException:
            print("Cannot load model from {}".format(filename))
            exit()
        self.model.load_state_dict(checkpoint['model'])
        self.opt = checkpoint['config']

    def save(self, filename, epoch):
        params = {
                'model': self.model.state_dict(),
                'config': self.opt,
                }
        try:
            torch.save(params, filename)
            print("model saved to {}".format(filename))
        except BaseException:
            print("[Warning: Saving failed... continuing anyway.]")

class GCNTrainer(Trainer):
    def __init__(self,supports,scalar,opt):
        super(GCNTrainer, self).__init__()
        self.opt = opt
        num_nodes = opt['num_nodes']
        addaptadj = opt['addaptadj']
        randomadj = opt['randomadj']
        if randomadj:
            aptinit = None
        else:
            aptinit = supports[0]

        self.model = gwnet(supports,opt,num_nodes,addaptadj=addaptadj,aptinit=aptinit)
        self.scalar=scalar
        self.loss_func= opt['loss_func']
        # total_params = sum(p.numel()for p in self.model.parameters() if p.requires_grad)
        # print('all trainable parameters :',total_params)
        # for name,param in self.model.named_parameters():
        #     if param.requires_grad:
        #         print(name,param.data.size())

        if opt['cuda']:
            # torch.backends.cudnn.benchmark = True
            torch.cuda.manual_seed(opt['seed'])
            self.model.cuda()

        self.optimizer = optim.Adam(self.model.parameters(),lr=opt['lr'],weight_decay=0.0001)
        self.clip =5

    def train(self,inputs,real_val):
        # step forward
        self.model.train()
        self.optimizer.zero_grad()
        predict = self.model(inputs)
        # predict shape:[B,T,N,1] real_val shape[B,T,N]
        real_val = torch.unsqueeze(real_val,dim=-1)
        loss=self.get_loss(real_val,predict)
        loss_val = float(loss.item())
        # backward
        loss.backward()
        if self.clip is not None:
            torch.nn.utils.clip_grad_norm_(self.model.parameters(), self.opt['max_grad_norm'])
        self.optimizer.step()
        return loss_val

    def predict(self,inputs,real_val):
        # forward
        self.model.eval()
        predict = self.model(inputs)
        real_val = torch.unsqueeze(real_val, dim=-1)
        # labels = torch.squeeze(labels)
        loss = self.get_loss(real_val,predict)
        return predict,float(loss.item())

    def get_loss(self,labels,preds):
        if self.loss_func == 'mae':
            loss = metrics.masked_mae_loss(self.scalar)(grads=labels, preds=preds, null_val=0.0)
        elif self.loss_func == 'rmse ':
            loss = metrics.masked_rmse_loss(self.scalar)(grads=labels, preds=preds, null_val=0.0)
        else:  # mape
            loss = metrics.masked_mape_loss(self.scalar)(grads=labels, preds=preds, null_val=0.0)
        return  loss