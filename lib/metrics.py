# coding=utf-8
import numpy as np
import torch
import time


# MAPE:Mean Absolute Perentage Error
from lib.modeldevice import device


def mape_loss(grads,preds,null_val):
    if np.isnan(null_val):
        mask = ~torch.isnan(grads)
    else:
        mask = ~torch.eq(grads, null_val)
    mask = mask.type(torch.FloatTensor).to(device)
    mask /= torch.mean(mask)
    mask = torch.where(torch.isnan(mask), torch.zeros_like(mask), mask)
    loss = torch.abs(torch.div(grads-preds , grads))
    loss = loss * mask
    loss = torch.where(torch.isnan(loss), torch.zeros_like(loss), loss)
    loss = torch.mean(loss)
    return loss

def masked_mape_loss(scaler):
    def loss(grads,preds,null_val):
        if scaler:
            preds = scaler.inverse_transform(preds)
            grads = scaler.inverse_transform(grads)
        mape = mape_loss(grads=grads,preds=preds,null_val=null_val)
        return mape
    return loss

# MAE:Mean Absolute Error
def mae_loss(grads,preds,null_val):
    if np.isnan(null_val):
        mask = ~torch.isnan(grads)
    else:
        mask = ~torch.eq(grads, null_val)
    mask = mask.type(torch.FloatTensor).to(device)
    mask /= torch.mean(mask)
    mask=torch.where(torch.isnan(mask),torch.zeros_like(mask),mask)
    loss = torch.abs(grads-preds)
    loss = loss * mask
    loss = torch.where(torch.isnan(loss), torch.zeros_like(loss), loss)
    return torch.mean(loss)

def masked_mae_loss(scaler):
    def loss(grads,preds,null_val):
        if scaler:
            preds = scaler.inverse_transform(preds)
            grads = scaler.inverse_transform(grads)
        mae = mae_loss(grads=grads,preds=preds,null_val=null_val)
        return mae
    return loss

# RMSE:Root Mean Square Error
def rmse_loss(grads,preds,null_val):
    if np.isnan(null_val):
        mask = ~torch.isnan(grads)
    else:
        mask = ~torch.eq(grads, null_val)
    mask = mask.type(torch.FloatTensor).to(device)
    mask /= torch.mean(mask)
    mask = torch.where(torch.isnan(mask), torch.zeros_like(mask), mask)
    loss = (grads-preds)**2
    loss = loss * mask
    loss = torch.where(torch.isnan(loss), torch.zeros_like(loss), loss)
    loss = torch.sqrt(torch.mean(loss))
    return loss

def masked_rmse_loss(scaler):
    def loss(grads,preds,null_val):
        if scaler:
            preds = scaler.inverse_transform(preds)
            grads = scaler.inverse_transform(grads)
        rmse = rmse_loss(grads=grads,preds=preds,null_val=null_val)
        return rmse
    return loss

def masked_mse_np(preds, labels, null_val=np.nan):
    with np.errstate(divide='ignore', invalid='ignore'):
        if np.isnan(null_val):
            mask = ~np.isnan(labels)
        else:
            mask = np.not_equal(labels, null_val)
        mask = mask.astype('float32')
        mask /= np.mean(mask)
        rmse = np.square(np.subtract(preds, labels)).astype('float32')
        rmse = np.nan_to_num(rmse * mask)
        return np.mean(rmse)


def masked_mae_np(preds, labels, null_val=np.nan):
    with np.errstate(divide='ignore', invalid='ignore'):
        if np.isnan(null_val):
            mask = ~np.isnan(labels)
        else:
            mask = np.not_equal(labels, null_val)
        mask = mask.astype('float32')
        mask /= np.mean(mask)
        mae = np.abs(np.subtract(preds, labels)).astype('float32')
        mae = np.nan_to_num(mae * mask)
        return np.mean(mae)


def masked_mape_np(preds, labels, null_val=np.nan):
    with np.errstate(divide='ignore', invalid='ignore'):
        if np.isnan(null_val):
            mask = ~np.isnan(labels)
        else:
            mask = np.not_equal(labels, null_val)
        mask = mask.astype('float32')
        mask /= np.mean(mask)
        mape = np.abs(np.divide(np.subtract(preds, labels).astype('float32'), labels))
        mape = np.nan_to_num(mask * mape)
        return np.mean(mape)

def masked_rmse_np(preds, labels, null_val=np.nan):
    return np.sqrt(masked_mse_np(preds=preds, labels=labels, null_val=null_val))

def calculate_metrics(df_pred, df_test, null_val):
    """
    Calculate the MAE, MAPE, RMSE
    :param df_pred:
    :param df_test:
    :param null_val:
    :return:
    """
    mape = masked_mape_np(preds=df_pred, labels=df_test, null_val=null_val)
    mae = masked_mae_np(preds=df_pred, labels=df_test, null_val=null_val)
    rmse = masked_rmse_np(preds=df_pred, labels=df_test, null_val=null_val)
    return  mape,rmse,mae

def calculate_torch_metrics(df_pred, df_test):
    mae = mae_loss(df_test,df_pred)
    time.sleep(50)
    rmse = rmse_loss(df_test,df_pred)
    time.sleep(50)
    mape = mape_loss(df_test, df_pred)
    return  mape,rmse,mae


