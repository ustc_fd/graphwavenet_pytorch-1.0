# coding=utf-8
import argparse
from collections import OrderedDict
import torch
import os
import pandas as pd
from lib import metrics, utils
from lib.modeldevice import device
from trainer import GCNTrainer


def predict_singlegpu(support,opt,pred_dataloader,df_test,scaler,write_db=False):
    # load pretrained model
    model_file = opt['save_dir'] + '/' + 'adgcn'+'/best_model.pt'
    print("Loading model from {}".format(model_file))
    trainer = GCNTrainer(support, scaler, opt)
    trainer.load(model_file)

    final_output = []
    with torch.no_grad():
        for iter,(x, y) in enumerate(pred_dataloader.get_iterator()):
            test_x, test_y = torch.Tensor(x).to(device), torch.Tensor(y).to(device)
            test_x = test_x.transpose(1, 3)
            pred, _ = trainer.predict(test_x,test_y[:,:,:,0])
            #pred size:(batch_size,num_node,pred_len)
            final_output.append(pred)
       # preds shape:[batch_size*batch_len,T,N,...]
        preds = torch.cat(final_output, 0)
        print(preds.size())
        # num_test=preds.size(0)
        # df_test原始数据
        # eval_df = utils.prepare_eval_df(df_test,opt.seq_len,opt.pred_len,num_test)
        pred_data = preds.data.cpu().numpy()
        # preds_df=utils.convert_model_outputs_to_eval_df(pred_data,scaler,opt.pred_len,eval_df)

        predictions = []
        y_truths = []
        for horizon_i in range(df_test.shape[1]):
            y_truth = scaler.inverse_transform(df_test[:, horizon_i, :, 0])
            y_truths.append(y_truth)

            y_pred = scaler.inverse_transform(pred_data[:y_truth.shape[0], horizon_i, :, 0])
            predictions.append(y_pred)

            mape, rmse, mae = metrics.calculate_metrics(y_pred, y_truth, 0.0)
            print('(Predicting) Horizon {}, mape: {mape:3.3f},rmse:{rmse:3.3f},mae:{mae:3.3f}'
                 .format(horizon_i+1,mape=mape,rmse=rmse,mae=mae))

    print("Evaluation ended.")
           #
           # if write_db:
           #     log_pred_file = os.path.join(summaryDir, 'METR.pred.log')
           #     with open(log_pred_file, 'a') as log_tf:
           #         log_tf.write(
           #             'In {} step ahead forecasting,Horizon {} final 20% testdataset '
           #             'mape: {mape_loss:8.4f},rmse:{rmse_loss:8.4f},mae:{mae_loss:8.4f}\n'
           #             .format(12,horizon_i+1,mape_loss=mape,rmse_loss=rmse,mae_loss=mae))
               # if opt.output_preds:
               #     df_pred = pd.DataFrame(df_pred, index=df_test.index, columns=df_test.columns)
               #     filename = os.path.join('data','results', 'model_prediction_%d.h5' % (horizon_i + 1))
               #     df_pred.to_hdf(filename, 'results')




if __name__ == '__main__':
    from lib.utils import load_graph_data, load_dataset
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default='data/METR-LA')
    parser.add_argument('--graph_pkl_filename', type=str, default='data/sensor_graph/sensor_adj_mx.pkl')
    parser.add_argument('--batch_size', type=int, default=32, help='Training batch size.')
    parser.add_argument('--val_batch_size', type=int, default=32, help='Training batch size.')
    parser.add_argument('--save_dir', type=str, default='./saved_models', help='Root dir for saving models.')
    parser.add_argument('--seed', type=int, default=1234)
    parser.add_argument('--cuda', type=bool, default=torch.cuda.is_available())

    args = parser.parse_args()
    opt=vars(args)
    graph_pkl_filename = args.graph_pkl_filename
    adj_mx = load_graph_data(graph_pkl_filename)
    data = load_dataset(args.data_dir,args.batch_size,args.val_batch_size)
    scaler = data['scaler']
    df_test=data['y_test']
    test_dataloader=data['test_loader'].get_iterator()
    predict_singlegpu(adj_mx,opt,test_dataloader,df_test,scaler)



