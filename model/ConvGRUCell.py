
import torch
from torch import nn
from lib.metrics import device

class GRUCell(nn.Module):
    def __init__(self, input_size, hidden_size):
        super(GRUCell, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.ConvGates = nn.Linear(self.input_size + self.hidden_size, 2 * self.hidden_size)
        self.Conv_ct = nn.Linear(self.input_size + self.hidden_size, self.hidden_size)
        dtype = torch.FloatTensor

    def forward(self, input, hidden):
        c1 = self.ConvGates(torch.cat((input, hidden), -1))
        (rt, ut) = c1.chunk(2, -1)
        reset_gate = torch.sigmoid(rt)
        update_gate = torch.sigmoid(ut)
        gated_hidden = torch.mul(reset_gate, hidden)
        p1 = self.Conv_ct(torch.cat((input, gated_hidden), -1))
        ct = torch.tanh(p1)
        next_h = torch.mul(update_gate, hidden) + (1 - update_gate) * ct
        return next_h

    def init_hidden(self,batch_size,node_size):
        hidden_state=torch.zeros(batch_size,node_size, self.hidden_size).to(device)
        return hidden_state



if __name__ == '__main__':
    x=torch.rand(32,6,207,1)
    m=GRUCell(1,64)
    h_state=m.init_hidden(x.size(0),x.size(2))
    for i in range(6):
        inputs=x[:,i,...]
        h_state=m(inputs,h_state)
    print(h_state.size())



