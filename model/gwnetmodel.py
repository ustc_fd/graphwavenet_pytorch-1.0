import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from model.layers import STConv_Dilation_Layer, Addaptadj


class gwnet(nn.Module):
    def __init__(self,supports,model_kwargs,num_nodes,addaptadj = True,aptinit = None):
        super(gwnet,self).__init__()


        self.num_timesteps_input =model_kwargs['num_timesteps_input']
        self.num_timesteps_output = model_kwargs['num_timesteps_output']
        self.in_dims = model_kwargs['input_dims']
        self.residual_channels =model_kwargs['residual_channels']
        self.dilation_channels = model_kwargs['dilation_channels']
        self.skip_channels = model_kwargs['skip_channels']
        self.end_channels = model_kwargs['end_channels']

        self.num_residual_blocks = model_kwargs['num_residual_blocks']
        self.num_dilation_layers = model_kwargs['num_dilation_layers']
        self.kernel_t = model_kwargs['kernel_t']
        self.kernel_s = model_kwargs['kernel_s']

        self.supports = supports
        self.addaptadj = addaptadj

        support_len = len(supports) if isinstance(supports,list) else 1

        if self.addaptadj:
            self.construct_adj_fn = Addaptadj(num_nodes,aptinit)
            self.support_len = support_len +1
        else:
            self.support_len = support_len

        self.receptive_filed = self.calc_receptive_fields(self.num_dilation_layers,self.num_residual_blocks)
        self.diff_size = self.num_timesteps_input - self.receptive_filed
        if self.diff_size < 1:
            self.num_timesteps_input = self.num_timesteps_input + abs(self.diff_size) +1
        else:
            self.num_timesteps_input = self.num_timesteps_input


        self.start_conv = nn.Conv2d(in_channels=self.in_dims,
                                    out_channels=self.residual_channels,
                                    kernel_size=(1,1))

        self.stconv_layer =nn.ModuleList()
        current_receptive_filed = 0
        for b in range(self.num_residual_blocks):
            additional_scope = self.kernel_t -1
            new_dilation = 1
            current_receptive_filed += additional_scope
            num_timesteps_current = self.num_timesteps_input - current_receptive_filed
            self.stconv_layer.append(STConv_Dilation_Layer(residual_channel=self.residual_channels,
                                                           tconv_channel=self.dilation_channels,
                                                           kernel_t=self.kernel_t,
                                                           conv_dilation=new_dilation,
                                                           kernel_s=self.kernel_s,
                                                           support_len=self.support_len,
                                                           skip_channel=self.skip_channels,
                                                           num_nodes=num_nodes,
                                                           timesteps_current = num_timesteps_current,
                                                           use_agmented_conv=True,
                                                           use_norm=True
                                                           ))

            additional_scope += new_dilation * additional_scope

        self.final_timestep = self.num_timesteps_input - current_receptive_filed

        self.end_conv_1 = nn.Conv2d(in_channels=self.skip_channels,
                                      out_channels=self.end_channels,
                                      kernel_size=(1,self.final_timestep))
        self.end_conv_2 = nn.Conv2d(in_channels=self.end_channels,
                                    out_channels=self.num_timesteps_output,
                                    kernel_size=(1,1))


    @staticmethod
    def calc_receptive_fields(layer_size, stack_size):
        layers = [2 ** i for i in range(0, layer_size)] * stack_size
        num_receptive_fields = np.sum(layers)
        return int(num_receptive_fields)

    def forward(self, inputs):

        if self.addaptadj:
            addaptadj = self.construct_adj_fn()
            supports = self.supports+[addaptadj]
        else:
            supports = self.supports

        if self.diff_size < 1:
            x = F.pad(inputs,(abs(self.diff_size)+1,0,0,0))
        else:
            x = inputs

        x = self.start_conv(x)
        skips = 0
        for i in range(self.num_residual_blocks):
            x,skips = self.stconv_layer[i](x,skips,supports)

        # skip = torch.stack(skips,dim=0)
        # skip = torch.sum(skip,dim=0)
        x = F.relu(skips)
        x = F.relu(self.end_conv_1(x))
        x = self.end_conv_2(x)
        return  x