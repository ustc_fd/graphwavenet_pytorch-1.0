import torch
import torch.nn as nn
import numpy as np
import torch.nn.functional as F

from lib.modeldevice import device
from model.attentionConv2d import AugmentedConv2d


class nconv(nn.Module):
    def __init__(self):
        super(nconv,self).__init__()
    def forward(self,x,A):
        dims_A = len(list(A.size()))
        # if dims_A == 2:
        #     einsum_eq = "nm,btmd->btnd"
        # elif dims_A == 3:
        #     einsum_eq = "bnm,btmd->btnd"
        # else:
        #     raise ValueError(f'ERROR: einsum equation is not defined.')
        #
        # x = x.transpose(1,3)#[B,T,N,C]
        # x = torch.einsum(einsum_eq,(A,x))
        # x = x.transpose(1,3)#[B,C,N,T]

        # if dims_A == 2:
        #     einsum_eq = "nm,bdmt->bdnt"
        # elif dims_A == 3:
        #     einsum_eq = "bnm,bdmt->bdnt"
        # else:
        #     raise ValueError(f'ERROR: einsum equation is not defined.')
        # x = torch.einsum(einsum_eq,(A,x))


        if dims_A == 2:
            einsum_eq = "bcnt,nm->bcmt"
        elif dims_A == 3:
            einsum_eq = "bcnt,bnm->bcmt"
        else:
            raise ValueError(f'ERROR: einsum equation is not defined.')
        x = torch.einsum(einsum_eq,(x,A))
        return x.contiguous()

class gcn(nn.Module):
    def __init__(self,in_channels,out_channels,support_len=3,max_hops=2):
        super(gcn,self).__init__()
        self.nconv =nconv()
        c_in = (max_hops * support_len + 1) * in_channels
        self.mlp = nn.Conv2d(in_channels=c_in,out_channels=out_channels,kernel_size=(1,1))
        self.max_hops = max_hops
    def forward(self,x,adj_supports):
        if not isinstance(adj_supports,list):
            adj_supports = [adj_supports]
        outputs = [x]
        for adj in adj_supports:
            x1 = self.nconv(x,adj)
            outputs.append(x1)
            for k in range(2,self.max_hops+1):
                x2 =self.nconv(x1,adj)
                outputs.append(x2)
                x1 =x2
        h = torch.cat(outputs,dim=1)
        h = self.mlp(h)
        return h

class timecov(nn.Module):
    def __init__(self,in_channel,out_channel,kernel_size,new_dilation=1,padding_flag=False):
        super(timecov,self).__init__()
        self.padding_flag = padding_flag
        self.padding_odd = (new_dilation * (kernel_size - 1) % 2 != 0)
        if padding_flag:
            padding_size =int(new_dilation*(kernel_size-1)/2)
        else:
            padding_size =0

        self.out_conv = nn.Conv2d(in_channels=in_channel,
                             out_channels=out_channel,
                             kernel_size=(1,kernel_size),
                             dilation=(1,new_dilation),
                             padding=(0,padding_size))
    def forward(self, x):
        if self.padding_flag and self.padding_odd:
            x = F.pad(x,(1,0,0,0))
        h = self.out_conv(x)
        return h

class gated_tcn(nn.Module):
    def __init__(self, in_channel,out_channel,kernel_size,new_dilation,padding_flag):
        super(gated_tcn, self).__init__()
        self.filter_conv = timecov(in_channel = in_channel,
                                   out_channel=out_channel,
                                   kernel_size = kernel_size,
                                   new_dilation = new_dilation,
                                   padding_flag= padding_flag)
        self.gate_conv = timecov(in_channel = in_channel,
                                   out_channel=out_channel,
                                   kernel_size = kernel_size,
                                   new_dilation = new_dilation,
                                   padding_flag= padding_flag)

    def forward(self, x):
        filter = torch.tanh(self.filter_conv(x))
        gate = torch.sigmoid(self.gate_conv(x))
        x = filter * gate
        return x



class STConv_Dilation_Layer(nn.Module):
    def __init__(self,residual_channel,tconv_channel,kernel_t,conv_dilation,
                 kernel_s ,support_len, skip_channel, num_nodes,timesteps_current,
                 use_agmented_conv =False,use_norm=True):
        super(STConv_Dilation_Layer, self).__init__()
        self.use_norm = use_norm
        self.use_agmented_conv =use_agmented_conv
        self.temporal_conv = gated_tcn(in_channel=residual_channel,
                                   out_channel=tconv_channel,
                                   kernel_size = kernel_t,
                                   new_dilation = conv_dilation,
                                   padding_flag= False)

        self.gcn_conv = gcn(in_channels = tconv_channel,
                            out_channels = residual_channel,
                            support_len=support_len,
                            max_hops=kernel_s)

        if use_agmented_conv:
            self.augmented_atttention_conv = AugmentedConv2d(in_channels=residual_channel,out_channels=residual_channel,
                                                         kernel_size=1, dk=40, dv=16, Nh=4,
                                                         shape=[num_nodes,timesteps_current],relative=True)

        self.skip_connections = nn.Conv2d(in_channels=residual_channel,
                                          out_channels=skip_channel,
                                          kernel_size=(1,1))
        if use_norm:
            # self.norm_fn = nn.BatchNorm2d(residual_channel)
            self.norm_fn = nn.LayerNorm([num_nodes,residual_channel])

    def forward(self,x,skip,support_adj):
        residual = x
        x = self.temporal_conv(x)

        s = self.skip_connections(x)
        try:
            skip = skip[:,:,:,-s.size(3):]
        except:
            skip = 0

        skip = s + skip
        x = self.gcn_conv(x, support_adj)
        if self.use_agmented_conv:
            x = self.augmented_atttention_conv(x)
        x = x + residual[:,:,:,-x.size(3):]
        if self.use_norm:
            x = self.norm_fn(x.transpose(1,3))
            x = x.transpose(1,3)
            # x = self.norm_fn(x)
        return x,skip


class Addaptadj(nn.Module):
    def __init__(self,num_nodes,aptinit=None):
        super(Addaptadj, self).__init__()
        if aptinit is None:
            self.nodevec1 = nn.Parameter(torch.randn(num_nodes, 10), requires_grad=True).to(device)
            self.nodevec2 = nn.Parameter(torch.randn(10, num_nodes), requires_grad=True).to(device)
        else:
            m, p, n = torch.svd(aptinit)
            initemb1 = torch.mm(m[:, :10], torch.diag(p[:10] ** 0.5))
            initemb2 = torch.mm(torch.diag(p[:10] ** 0.5), n[:, :10].t())
            self.nodevec1 = nn.Parameter(initemb1, requires_grad=True).to(device)
            self.nodevec2 = nn.Parameter(initemb2, requires_grad=True).to(device)
    def forward(self):
        adp = torch.mm(self.nodevec1, self.nodevec2)
        adp = F.softmax(F.relu(adp),dim=1)
        return adp


if __name__ == '__main__':
    B, C, N, T = 2, 16, 207, 12
    new_dilation = 1
    Kt = 2
    a = torch.randn(size=(B, C, N, T))
    func = timecov(16,32,Kt,1,padding_flag=True)
    c = func(a)
    print(c.size())