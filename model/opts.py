import torch

from lib.modeldevice import device


def calculate_3d_normadj(adj_mx,type='L1',self_loop=False):
    batch, N = adj_mx.size(0), adj_mx.size(1)
    if self_loop:
        I = torch.eye(N, device=device).unsqueeze(0)
        adj_mx = adj_mx + I
    D_hat = torch.sum(adj_mx, dim=-1, keepdim=True)
    if type=='L1':
        D_hat = (D_hat + 1e-5) ** (-1.0)
        norm_adjmx = D_hat * adj_mx
        # norm_adjmx = norm_adjmx.transpose(1,2)
    elif type=='L2':
        D_hat = (D_hat + 1e-5) ** (-0.5)
        norm_adjmx = (D_hat*adj).transpose(1, 2) *(D_hat)
    return norm_adjmx


def get_norm_adjmx(adj_mx,type,self_loop):
    dims = len(list(adj_mx.shape))
    if dims == 2:
        adj_mx = adj_mx.unsqueeze(0)
        outs = calculate_3d_normadj(adj_mx,type,self_loop).squeeze(0)
    elif dims == 3:
        outs = calculate_3d_normadj(adj_mx,type,self_loop)
    else:
        raise ValueError("Please check the dims of matrix A")
    return outs




if __name__ == '__main__':
    import numpy as np
    a = np.random.randn(5,10,10)
    a =np.abs(a)
    adj = torch.from_numpy(a)
    adj = torch.randn(5,10,10)
    adj = torch.abs(adj)
    print(adj)
    res = get_norm_adjmx(adj,type='L1',self_loop=True)
    print(res)