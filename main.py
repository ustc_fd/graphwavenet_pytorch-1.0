import argparse
import datetime
import time
import torch

import predicter
from lib import  helper
import lib.utils as utils
from lib.modeldevice import device
from trainer import GCNTrainer
import numpy as np
import random

def get_config():
    parser = argparse.ArgumentParser()
    # data
    parser.add_argument('--data_dir', type=str, default='data/METR-LA')
    parser.add_argument('--graph_pkl_filename', type=str, default='data/sensor_graph/adj_mx.pkl')
    parser.add_argument('--num_epoch', type=int, default=80, help='Number of total training epochs.')
    parser.add_argument('--batch_size', type=int, default=64, help='Training batch size.')
    parser.add_argument('--val_batch_size', type=int, default=64, help='Training batch size.')

    # model
    parser.add_argument('--adj_filter_type', type=str, default='doubletransition', help='dual or singele directions',
                        choices=['scalap', 'normlap', 'symnadj','transition','doubletransition','identity'])
    parser.add_argument('--addaptadj',type=bool, default=False, help='whether add adaptive adj')
    parser.add_argument('--randomadj',type=bool, default=False, help='whether random initialize adaptive adj')

    parser.add_argument('--input_dims', type=int, default=2, help='input features dimension.')
    parser.add_argument('--out_dims', type=int, default=1, help='out features dimension.')
    parser.add_argument('--num_nodes', type=int, default=207, help='adj max nodes size.')
    parser.add_argument('--kernel_t', type=int, default=3, help='CNN temporal convoution kernel.')
    parser.add_argument('--kernel_s', type=int, default=3, help='graph convoution kernel.')
    parser.add_argument('--num_timesteps_input', type=int, default=12, help='historal time steps.')
    parser.add_argument('--num_timesteps_output', type=int, default=12, help='prediction time steps.')
    parser.add_argument('--residual_channels', type=int, default=32, help='residual_channels')
    parser.add_argument('--dilation_channels', type=int, default=32, help='dilation_channels')
    parser.add_argument('--skip_channels', type=int, default=256, help='skip_channels')
    parser.add_argument('--end_channels', type=int, default=512, help='end_channels')
    parser.add_argument('--num_residual_blocks', type=int, default=4, help='num_residual_blocks')
    parser.add_argument('--num_dilation_layers', type=int, default=2, help='num_dilation_layers')


    # train
    parser.add_argument('--loss_func', type=str, default='mae', choices=['mae', 'mape', 'rmse'], help='Loss Function.')
    parser.add_argument('--earlystop_epochs', type=int, default=20, help='max early stop epochs.')
    parser.add_argument('--test_every_n_epochs', type=int, default=20, help='max early stop epochs.')
    parser.add_argument('--lr', type=float, default=0.001, help='Applies to sgd and adagrad.')
    parser.add_argument('--min_lr', type=float, default=1e-5, help='min learting rate.')
    parser.add_argument('--lr_decay', type=float, default=0.9, help='Learning rate decay rate.')
    parser.add_argument('--decay_epoch', type=int, default=5, help='Decay learning rate after this epoch.')
    parser.add_argument('--optim', default='adam',help='Optimizer: sgd, adagrad, adam or adamax.')
    parser.add_argument('--max_grad_norm', type=float, default=5.0, help='Gradient clipping.')
    parser.add_argument('--log', type=str, default='logs.txt', help='Write training log to file.')

    # dir
    parser.add_argument('--save_dir', type=str, default='./saved_models', help='Root dir for saving models.')
    parser.add_argument('--model_file', type=str, help='Filename of the pretrained model.')
    parser.add_argument('--load', dest='load', action='store_true', help='Load pretrained model.')

    parser.add_argument('--seed', type=int, default=1234)
    parser.add_argument('--cuda', type=bool, default=torch.cuda.is_available())

    args = parser.parse_args()
    opt = vars(args)
    return opt

def adjust_learning_rate(epoch,steps,initial_lr,lr_decay_ratio=0.1,min_lr=5e-6):
    # 每10个批次衰减百分之十
    # **幂 - 返回x的y次幂.//取整除 - 返回商的整数部分
    new_lr = max(min_lr, initial_lr * (lr_decay_ratio ** np.sum(epoch >= np.array(steps))))
    return new_lr

def train(opt):
    torch.manual_seed(opt['seed'])
    np.random.seed(opt['seed'])
    random.seed(1234)
    model_save_dir = opt['save_dir'] + '/' + 'adgcn'
    opt['model_save_dir'] = model_save_dir
    helper.ensure_dir(model_save_dir, verbose=True)

    file_logger = helper.FileLogger(opt['save_dir'] + '/' + opt['log'],
                                    header="# epoch\ttrain_loss\tdev_loss")
    # print model info
    helper.print_config(opt)

    graph_pkl_filename = opt['graph_pkl_filename']
    adj_filter_type = opt['adj_filter_type']
    sensor_ids,sensor_id_to_ind,adj_mx = utils.load_graph_data(graph_pkl_filename,adj_filter_type)

    dataloader = utils.load_dataset(opt['data_dir'],opt['batch_size'],opt['val_batch_size'])
    scaler = dataloader['scaler']
    test_dataloader,df_test = dataloader['test_loader'],dataloader['y_test']
    supports =[torch.tensor(i).to(device) for i in adj_mx]

    # model
    # if not opt['load']:
    #     trainer = GCNTrainer(supports,scaler,opt)
    # else:
    #     # load pretrained model
    #     model_file = opt['model_file']
    #     print("Loading model from {}".format(model_file))
    #     trainer = GCNTrainer(supports,scaler,opt)
    #     trainer.load(model_file)

    trainer = GCNTrainer(supports, scaler, opt)
    # start training
    dev_loss_history = []
    current_lr = opt['lr']
    test_every_n_epochs=opt['test_every_n_epochs']
    print('starting training  ......')
    start_train_time=int(time.time())
    print('start time:',datetime.datetime.fromtimestamp(start_train_time))
    for epoch in range(1, opt['num_epoch'] + 1):
        print('Epoch {}/{}'.format(epoch, opt['num_epoch']))
        print('-' * 10)
        start_time = time.time()
        train_loss,valid_loss =[],[]
        dataloader['train_loader'].shuffle()
        for iter,(x,y) in enumerate(dataloader['train_loader'].get_iterator()):
            train_x ,train_y = torch.Tensor(x).to(device), torch.Tensor(y).to(device)
            train_x = train_x.transpose(1,3)#[B.C.N,T]
            loss = trainer.train(train_x ,train_y[:,:,:,0])
            train_loss.append(loss)

        duration = time.time() - start_time
        # eval on dev
        print("Evaluating on dev set...")
        for iter, (x, y) in enumerate(dataloader['val_loader'].get_iterator()):
            val_x, val_y = torch.Tensor(x).to(device), torch.Tensor(y).to(device)
            val_x = val_x.transpose(1, 3)
            _,loss = trainer.predict(val_x, val_y[:,:,:,0])
            valid_loss.append(loss)

        loss_train = np.mean(train_loss)
        loss_val = np.mean(valid_loss)
        print("epoch {}: train_loss = {:.4f}, dev_loss = {:.4f},epoch time:{:.3f},({:.3f} sec/batch), lr: {:.6f}".
              format(epoch,loss_train,loss_val,duration,duration/len(train_loss), current_lr))
        file_logger.log("{}\t{:.4f}\t{:.4f}".format(epoch, loss_train, loss_val))

        # save
        if epoch == 1 or loss_val< min(dev_loss_history):
            earlystop_epoch=0
            model_file = model_save_dir + '/best_model.pt'
            trainer.save(model_file, epoch)
            print("new best model saved.")
            file_logger.log("new best model saved at epoch {}: train loss:{:.4f}\t val loss:{:.4f}" \
                            .format(epoch, loss_train, loss_val))
        else:
            earlystop_epoch+=1
            if earlystop_epoch>=opt['earlystop_epochs']:
                print('the best loss has not been better when at {} epochs'.format(epoch))
                break

        if epoch % test_every_n_epochs == test_every_n_epochs - 1:
            predicter.predict_singlegpu(supports, opt, test_dataloader, df_test, scaler)

        # lr schedule
        current_lr = adjust_learning_rate(epoch, steps=[20,35,50,60], initial_lr=opt['lr'])
        trainer.update_lr(current_lr)
        # if len(dev_loss_history) > opt['decay_epoch'] and dev_loss >= dev_loss_history[-1]:
        #         # and opt['optim'] in ['sgd', 'adagrad', 'adadelta']:
        #     schedule_lr = current_lr*opt['lr_decay']
        #     current_lr=max(opt['min_lr'],schedule_lr)
        #     trainer.update_lr(current_lr)

        dev_loss_history += [loss_val]

    predicter.predict_singlegpu(supports, opt, test_dataloader, df_test, scaler)
    print("Training ended with {} epochs.".format(epoch))
    end_train_time=int(time.time())
    print('start time:', datetime.datetime.fromtimestamp(end_train_time))

if __name__ == '__main__':
    opt=get_config()
    train(opt)
